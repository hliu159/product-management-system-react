import * as React from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import EditIcon from '@mui/icons-material/Edit';
import Paper from '@mui/material/Paper';
import DeleteIcon from '@mui/icons-material/Delete';
import { Dialog, DialogActions, DialogContent, DialogTitle, Button} from '@mui/material';
import {IconButton} from '@mui/material';
import DoneIcon from '@mui/icons-material/Done';
import CancelIcon from '@mui/icons-material/Cancel';
import UploadIcon from '@mui/icons-material/Upload';
import { TextField } from '@mui/material';
import { visuallyHidden } from '@mui/utils';
import { useState , useEffect } from 'react';
import Header from './Header';
import { apiPost, apiDelete } from './Service';
import AddIcon from '@mui/icons-material/Add'
import ImportExcel from './ImportExcel';
import ExportExcel from './ExportExcel';

const getComparator = (order, orderBy) => {
  return (a, b) => {
    if (b[orderBy] === a[orderBy]) {
      return 0;
    } else if (a[orderBy] == null || a[orderBy] === "") {
      return 1;
    } else if (b[orderBy] == null || b[orderBy] === "") {
      return -1;
    } else if (orderBy === "price") {
      if (order === "asc") {
        return parseInt(a[orderBy], 10) < parseInt(b[orderBy], 10) ? -1 : 1
      } else {
        return parseInt(b[orderBy], 10) > parseInt(a[orderBy], 10) ? 1 : -1
      }
    } else if (order === "asc" && orderBy !== "price") {
      return a[orderBy].toUpperCase() < b[orderBy].toUpperCase() ? -1 : 1;
    } else {
      return a[orderBy].toUpperCase() < b[orderBy].toUpperCase() ? 1 : -1;
    }
  }
}

const headCells = [
  {
    id: 'title',
    numeric: false,
    label: 'Title',
  },
  {
    id: 'description',
    numeric: true,
    label: 'Description',
  },
  {
    id: 'price',
    numeric: true,
    label: 'Price($)',
  }
];

function EnhancedTableHead(props) {
  const { order, orderBy, onRequestSort } =
    props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={"check-box"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
        <TableCell align="right" padding={"check-box"}>
            Product Image
          </TableCell>
          <TableCell align="right" padding={"check-box"}>
            Action
          </TableCell>
      </TableRow>
    </TableHead>
  );
}

//主程序
export default function EnhancedTable({ data, message, setMessage }) {
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [edit, setEdit] = useState('');
  const [dialogStatus,setDialogStatus] = useState(false);
  const [dialog, setDialog] = useState({})
  const [selectedFile, setSelectedFile] = useState()
  const [picture, setPicture] = useState(null)
  const [preview, setPreview] = useState()
  const [tableCellEdit, setTableCellEdit] = useState({})
  const [products, setProducts] = useState([]);
  const [searchedValue, setSearchedValue] = useState('')
  let formData = new FormData()
  const [newProduct, setNewProduct] = useState({})
  const [onAddNew, setOnAddNew] = useState(false)



    // 搜索数据筛选
      useEffect(() => {
        setPage(0)
        setProducts(data.filter(product => {
          if (searchedValue === '') {
            return product;
          } else if (product.title && searchedValue && product.title.toLowerCase().includes(searchedValue.toLowerCase())) {
            return product;
          } else if (product.description && searchedValue && product.description.toLowerCase().includes(searchedValue.toLowerCase())) {
            return product;
          }
        }))
      }, [searchedValue, data])

      // create the picture to URL of user uploading Image in preview
      useEffect(() => {
        if (!selectedFile) {
          setPreview(undefined)
          return
        }
        const objectUrl = URL.createObjectURL(selectedFile)
        setPreview(objectUrl)
      }, [selectedFile])

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };


  //删除对话框打开
  const handleClickOpen = prop => {
    setDialogStatus(true);
    setDialog(prop)
  }

  //删除对话框关闭
  const handleClose = () => {
    setDialogStatus(false);
  }

  //接收返回的修改数据值
  const handleChange = e => {
    setTableCellEdit({ ...tableCellEdit, [e.target.name]: e.target.value })
  }

  //接收返回的新增数据值
  const handleAddNew = e => {
    setNewProduct({ ...newProduct, [e.target.name]: e.target.value })
  }

  //添加数据
  const createProduct = (data) => {
    setOnAddNew(!onAddNew)
    formData.append('title', data.title)
    formData.append('description', data.description)
    formData.append('price', data.price)
    formData.append('category_id',1)
    formData.append('is_active',1)
    if (picture) {
      formData.append('product_image', picture)
    }
    apiPost('products', formData).then(res => {
      setProducts([res.data, ...products])
      setEdit('')
      setPicture('')
      setPage(0)
      res.data ? setMessage('createdNew') : setMessage('notCreatedNew')
    })
  }

  //删除数据
  const onDelete = (id) => {
    apiDelete(`product/${id}`).then(res => {
      res.data && setProducts(products.filter((product) => product.id !== id))
      res.data ? setMessage('delete-success') : setMessage('delete-unsuccess')
    })
  }

  //修改数据
  const handleData = data => {
    formData.append('title', data.title)
    formData.append('description', data.description)
    formData.append('price', data.price)
    formData.append('category_id',1)
    formData.append('is_active',1)
    formData.append('_method', 'PUT');
    if (picture) {
      formData.append('product_image', picture)
    }
      apiPost(`product/${data.id}`, formData).then(res => {
        setEdit('')
        setProducts(products.map((product) => product.id === res.data.id ? res.data : product))
        setPicture('')
        res.data ? setMessage('updated') : setMessage('notUpdated')
      })
  }

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - products.length) : 0;

  return (
    <>
    <Header setSearchedValue={setSearchedValue} message={message} setMessage={setMessage}/>
    <Box sx={{ mt: 2, maxWidth: { xs: 450, sm: 550, md: 1050, xl: 1450 }, ml: "auto", mr: "auto"}}>

      {/* 添加按钮 */}
    <Box sx={{ fontSize: 15, display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
            <Box sx={{ display: 'flex' }} ><p>Add New</p><IconButton variant="outlined" onClick={() => setOnAddNew(true)} sx={{ px: 1.5 }}><AddIcon /></IconButton></Box>
            <Box sx={{ display: { xs: "flex" } }} >
            </Box>

      {/* Excel功能 */}
    <Box sx={{ display: { xs: "flex" } }} >
              <ExportExcel Products={products} setMessage={setMessage} />
              <ImportExcel Products={products} setProducts={setProducts} setMessage={setMessage} />
            </Box>
      </Box>

      <Paper sx={{ mt: 3, width: '100%', overflow: 'hidden' }}>
        <TableContainer >
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
          >
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={products.length}
            />
            <TableBody>

              {/* 添加行 */}
              {onAddNew &&
            <TableRow >
      <TableCell component="th" scope="row">
        <TextField
          name='title'
          onChange={e => {
            handleAddNew(e)
          }}
        />
      </TableCell>
      <TableCell align="right" >
        <TextField
          name='description'
          onChange={e => {
            handleAddNew(e)
          }}
        />
      </TableCell>
      <TableCell align="right">
        <TextField
          type='number'
          name='price'
          InputProps={{ inputProps: { min: 0 } }}
          onChange={e => {
            handleAddNew(e)
          }}
        />
      </TableCell>
      <TableCell align="right">
        <Box
          sx={{
            display: "inline-block",
            position: 'relative',
          }}>
          <Box sx={{
            display: "flex"
          }}>
            {preview && <Box
              component="img"
              sx={{
                height: 93,
                width: 200,
                maxHeight: { xs: 93, md: 37 },
                maxWidth: { xs: 200, md: 100 },
              }} src={preview} />}
            <Box>
              <IconButton component="label" htmlFor="addNew-file"><UploadIcon /></IconButton>
              <input hidden name="product_image" accept="image/*" id="addNew-file" multiple type="file" onChange={(e) => {
                setSelectedFile(e.target.files[0])
                setPicture(e.target.files[0])
              }} />
            </Box>
          </Box>
        </Box>
      </TableCell>
      <TableCell align="right">
        <Box sx={{
          display: "flex",
          justifyContent: "flex-end",

        }}>
          <IconButton
            disabled={!newProduct.title || !newProduct.description || !newProduct.price}
            variant="outlined"
            size="medium"
            onClick={() => {
              createProduct(newProduct)
              setPreview(undefined)
            }} ><DoneIcon /></IconButton>
          <IconButton variant="outlined" onClick={() => {
            setOnAddNew(!onAddNew)
            setPreview(undefined)
          }
          }><CancelIcon /></IconButton>
        </Box>
      </TableCell>
    </TableRow >
      }

            {/* 展示数据 */}
              {products.sort(getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  return (
                    <TableRow key={index} id={row.id}>
                      <TableCell>
                    {edit === row.id ?
                      <TextField
                        name='title'
                        defaultValue={row.title}
                        onChange={e => {
                          handleChange(e)
                        }}
                      />
                      : <div>{row.title}</div>
                      }
                      </TableCell>
                      <TableCell align="right">
                      {edit === row.id ?
                        <TextField
                          name='description'
                          defaultValue={row.description}
                          onChange={e => {
                            handleChange(e)
                          }}
                        />
                        : <div>{row.description}</div>
                      }
                        </TableCell>
                      <TableCell align="right">
                      {edit === row.id ?
                        <TextField
                          type='number'
                          name='price'
                          InputProps={{ inputProps: { min: 0 } }}
                          defaultValue={row.price}
                          onChange={e => {
                            handleChange(e)
                          }}
                        />
                        : <div>{row.price}</div>
                      }
                       </TableCell>

                {/* 图片上传 */}
                      <TableCell align="right">
                      <Box
                            sx={{
                              display: "flex",
                              justifyContent: "flex-end",
                              verticalAlign: "middle",
                            }}>
                            {row.product_image &&
                              <Box>
                                <Box
                                  component="img"
                                  sx={{
                                    height: 93,
                                    width: 200,
                                    maxHeight: { xs: 93, md: 37 },
                                    maxWidth: { xs: 200, md: 100 },
                                  }}
                                  src={preview && edit === row.id ? preview : `https://server.gradspace.org/storage/${row.product_image}`} 
                                />
                              </Box>}
                            {!row.product_image && preview && edit === row.id &&
                              <Box>
                                <Box
                                  component="img"
                                  sx={{
                                    height: 93,
                                    width: 200,
                                    maxHeight: { xs: 93, md: 37 },
                                    maxWidth: { xs: 200, md: 100 },
                                  }}
                                  src={preview} />
                              </Box>}
                            {!row.product_image && edit !== row.id && !preview &&
                              <Box sx={{
                                height: 133,
                                width: 250,
                                maxHeight: { xs: 133, md: 67 },
                                maxWidth: { xs: 250, md: 150 },
                              }}></Box>}
                            {edit === row.id &&
                              <Box>
                                <IconButton component="label" htmlFor="upload-file"><UploadIcon /></IconButton>
                                <input hidden id="upload-file" name="product_image" accept="image/*" multiple type="file" onChange={(e) => {
                                  setSelectedFile(e.target.files[0])
                                  setPicture(e.target.files[0])
                                }} />
                              </Box>
                            }
                          </Box>
                      </TableCell>
                      <TableCell align="right">
                        {edit === row.id ? 
                                      <Box sx={{
                                        display: "flex",
                                        justifyContent: "flex-end",
                        
                                      }}>
                                        <IconButton
                                          variant="outlined"
                                          size="medium"
                                          onClick={() => {
                                            handleData(tableCellEdit)
                                            setPreview(undefined)
                                            setEdit("")
                                          }} ><DoneIcon /></IconButton>
                                        <IconButton variant="outlined" onClick={() => {
                                          setEdit("")
                                          setPreview(undefined)
                                        }} ><CancelIcon /></IconButton>
                                      </Box>
                        :
                        <>
                        <EditIcon onClick={() => {
                            setEdit(row.id)
                            setTableCellEdit(() => row)
                              }} 
                          sx={{ mr: 2 }} style={{cursor:'pointer'}}/>
                        <DeleteIcon onClick={() => handleClickOpen(row)} style={{cursor:'pointer'}}/>
                        </>
                }
                      </TableCell>
                        
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={products.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>

      {/*删除窗口 */}
      <Dialog open={dialogStatus} >
        <DialogTitle >
          Data:{dialog.id}
        </DialogTitle>
        <DialogContent>
          Are you sure to delete?
        </DialogContent>
        <DialogActions>
          <Button onClick={() => {
            onDelete(dialog.id)
            handleClose()
          }}>
            Yes
          </Button>
          <Button onClick={handleClose}>
            No
          </Button>
        </DialogActions>
      </Dialog>
    </Box>
    </>
  );
}