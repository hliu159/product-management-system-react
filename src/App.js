import { useState , useEffect } from 'react'
import { Routes, Route, Navigate , useNavigate } from 'react-router-dom'
import { auth } from './components/Service'
import Table from './components/Table'
import LoginPage from './components/LoginPage'
import { apiGet, apiPost, apiPut, apiDelete } from './components/Service';
import SnackBar from './components/SnackBar'

function App() {
    const [data, setData] = useState([])
    const [message, setMessage] = useState("")

    useEffect(() => {
      apiGet(`products`)
      .then(res => setData(res.data))
      .catch(err => console.log(err))
    },[])
    
  return (
    <>
      <Routes>
        <Route path="/" element={<LoginPage message={message} setMessage={setMessage}/>} />
        <Route path="/products" element={auth() ? <Table data={data} message={message} setMessage={setMessage}/> : <Navigate to="/" />} />
      </Routes>
      <SnackBar message={message} setMessage={setMessage} />
    </>
  )

}

export default App;
