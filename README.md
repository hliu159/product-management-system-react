# Product Management System (React)  
  
You can view this project by visiting:  
https://prod.d21oy0ygi0s8xf.amplifyapp.com  
  
Account Information  
Email: jmliu159@gmail.com  
Password: QWER1234  
  
Functions  
•	Login and logout  
•	Realize automatic login function by reading and writing token into the browser’s local storage   
•	Search product by title, description and price  
•	Add, delete, edit and view products in the database  
•	Enable to upload the product image  
•	Automatic paging, which can control the number of entries displayed on each page  
•	Import/export product data via Excel file  
•	Display notification messages by the snack bar  
  
H Liu  
May 2023  

